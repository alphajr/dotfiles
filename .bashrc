##
## ~/.bashrc
##

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

# startx on login
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec startx
fi

# EXPORTS
#export PS1="\e[1;36m[\u@\h] \e[1;37m\W\e[m \$ \e[m"
export PS1=" \W > "                                     # bash prompt colors
export TERM="xterm-256color"                            # getting proper colors
#export MANPAGER="sh -c 'col -bx | bat -l man -p'"      # bat as manpager
export MANPAGER="nvim -c 'set ft=man' -"                # nvim as manpager



### My Aliases
alias wifi='nmtui'
alias vim='vim'
alias vi3='nvim ~/.config/i3/config'
alias vpolybar='nvim ~/.config/polybar/config'
alias neofetch='neofetch --source ~/.config/neofetch/asciiArts/thebat.txt'
alias install='sudo pacman -S'
alias config='/usr/bin/git --git-dir=/home/alphajr/dotfiles/ --work-tree=/home/alphajr' #git bare alias
alias unlock='sudo rm /var/lib/pacman/db.lck'

alias dmon='$HOME/.config/mysh/dualmon.sh' # dual monitor detection

# Changing "ls" to "exa"
alias ls='exa -l --color=always --group-directories-first' # my preferred listing
alias la='exa -al --color=always --group-directories-first'  # all files and dirs

# confirm before overwriting something
alias cp="cp -ig"
alias mv='mv -ig'
alias rm='rm -i'

# navigation
alias ..='cd ..' 
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

#exit
alias :q="exit"

### ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;      
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Rick Rolls
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

# Random Color Scripts
# colorscript -e thebat

neofetch --ascii_colors 4 --colors 4 4 4 4 7 7




export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
