# DotFiles

Arch Installation with i3-gaps and polybar.

![Desktop](https://raw.githubusercontent.com/shivajichalise/shivajichalise/main/images/mydesktop/desktop1.png)
![Neofetch,Htop & Cmatrix](https://raw.githubusercontent.com/shivajichalise/shivajichalise/main/images/mydesktop/desktop2.png)

Enable AUR:

```
git clone https://aur.archlinux.org/yay-git.git
cd yay-git
makepkg -si
```

Install i3-gaps and polybar:

```
pacman -S i3
yay -S polybar
```

Install required fonts:

```
pacman -S ttf-font-awesome
pacman -S ttf-ubuntu-font-family
yay -S nerd-fonts-mononoki
pacman -S xorg-fonts-misc
yay -S siji-git ttf-unifont
pacman -S unicode-character-database
```
